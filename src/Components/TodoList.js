import React, { useContext } from 'react';
import TodoContext from '../Context/TodoContext';

export default () => {
    const { todoState } = useContext(TodoContext);

    return (
        <>
            <h2>TodoList</h2>
            <ul>
                {todoState.todos.map(item=>(
                    <li key={item.objectID}>
                        <p>{item.text}</p>
                    </li>
                ))}
            </ul>
        </>

    )
}