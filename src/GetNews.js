import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';

export default () => {
    const [keyword, setKeyword] = useState("react");
    const [results, setResults] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);
    const searchInputRef = useRef();

    /*
    useRef returns a mutable ref object whose .current property is initialized to the passed argument (initialValue). 
    The returned object will persist for the full lifetime of the component.
    useRef的使用場景比較像是不受re-render影響的資料, 與生命週期沒有相關的資料
    */

    useEffect(()=>{
        getData();
    }, [])

    const getData = async () => {
        if (keyword === null || keyword === "")
            return;
        
        setIsLoading(true);

        try {
            const res = await axios.get(`http://hn.algolia.com/api/v1/search?query=${keyword}`);
            setResults(res.data.hits);
        }
        catch (error) {
            setError(error);
        }

        setIsLoading(false);
    }

    const clearKeyword = () => {
        setKeyword("");
        searchInputRef.current.focus();
    }

    return (
        <div>
            <h2>GetNews</h2>
            <input type='text' value={keyword} ref={searchInputRef} placeholder='search...' onChange={event=>setKeyword(event.target.value)} />
            <br /><br />
            <button type='submit' onClick={getData}>Submit</button>
            <button onClick={clearKeyword}>Clear</button>
            {isLoading ?
            (<div>Searing...</div>)
            :
            (<ul>
                {results.map(r=>(
                    <li key={r.objectID}>
                        <a href={r.url}>{r.title}</a>
                    </li>     
                ))}
            </ul>)}

            {error && <div>{error.message}</div>}
        </div>
    )
}