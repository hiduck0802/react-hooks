import React from 'react';

const TodosContext = React.createContext({
    todos: [
        { id:1, text: 'Eat', done: false},
        { id:2, text: 'Sleep', done: true},
        { id:3, text: 'Learn', done: false},
    ]
})

export default TodosContext;