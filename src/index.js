import React, { useContext, useReducer } from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import TodoContext from './Context/TodoContext';
import TodoReducer from './Reducer/TodoReducer';
import TodoList from './Components/TodoList';

/*
    Context api是為了解決每當A component要傳props給C component時, 若是中間隔了B component,
    即使Ｂ用不到但他還是會被經過
    到最後整包資料無腦的往下傳
    這個現象稱為'props drilling'
    Context api的用法是用Provicer包住需要props的child, 要向下傳的值包在provider的value裡面
    這樣就不會有'層層'的阻礙 :D
*/

const App = () => {
    const initialState = useContext(TodoContext);
    const [todoState, todoDispatch] = useReducer(TodoReducer, initialState);

    return (
        <TodoContext.Provider value={{todoState, todoDispatch}}>
            <TodoList />
        </TodoContext.Provider>
    )
}


ReactDOM.render(<App />, document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
