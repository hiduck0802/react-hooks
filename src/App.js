import React, { Component } from 'react';

class App extends Component {
  state = { 
    count: 0,
    lightMode: false,
    x: 0,
    y: 0
  }

  componentDidMount() {
    document.title = `Clicked ${this.state.count} times`;
    window.addEventListener('mouseover', this.mouseOverHandler);
  }

  componentDidUpdate() {
    document.title = `Clicked ${this.state.count} times`;
  }

  componentWillUnmount() {
    window.removeEventListener('mouseover', this.mouseOverHandler);
  }

  mouseOverHandler = event => {
    this.setState({
      x: event.pageX,
      y: event.pageY
    })
  }

  increaseCount = () => {
    this.setState(prevState => ({
      count: prevState.count+1
    }))
  };

  toggleLight = () => {
    this.setState(prevState => ({
      lightMode: !prevState.lightMode
    }))
  }


  render() { 
    const style = {
      backgroundColor: this.state.lightMode ? 'pink':'grey',
      font: 'inherit',
      border: '1px solid black',
      padding: '8px',
      cursor: 'pointer'
    };
  
    return (
      <>
        <button onClick={this.increaseCount}>Click me!</button>
        <div>Count: {this.state.count}</div>
        <h2>Toggle light</h2>
        <button style={style} onClick={this.toggleLight}>mode</button>
        <h2>Mouse Position</h2>
        <p>x:{this.state.x} Y:{this.state.y}</p>
      </>
      
    );
  }
}
 
export default App;
