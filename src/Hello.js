import React, { useContext } from 'react';
import { UserContext } from './index.js';

export default (props) => {
    const userContext = useContext(UserContext);
    return (
        <h2>Hello {userContext}</h2>
    )
}