import React, { useState } from 'react';

export default () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [userData, setUserData] = useState(null);

    const submitHandler = event => {
        event.preventDefault();
        const userData = {
            username,
            password
        }
        setUserData(userData);
        setUsername("");
        setPassword("");
    }

    const style = {
        display: 'grid',
        alignItems: 'center',
        justifyItems: 'center'
    }

    return (
        <div>
            <h2 style={{textAlign: 'center'}}>Login</h2>
            <form style={style} onSubmit={submitHandler}>
                <input type="text" placeholder="UserName" value={username} onChange={event => setUsername(event.target.value)}/>
                <input type="password" placeholder="Password" value={password} onChange={event => setPassword(event.target.value)}/>
                <button type="submit">Submit</button>
            </form>

            {userData && JSON.stringify(userData)}
        </div>
    )
}