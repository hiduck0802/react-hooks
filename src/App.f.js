import React, { useState, useEffect } from 'react';

const App = () => {
    const [count, setCount] = useState(0);
    const [lightMode, setLightMode] = useState(false);
    const [mousePosition, setMousePosition] = useState({x:null, y:null});

    useEffect(()=>{
        document.title=`Clicked ${count} times`;
        window.addEventListener('mouseover', mouseOverHandler);

        return (()=>{
            window.removeEventListener('mouseover', mouseOverHandler);
        })
    }, [count])

    // useEffect
    // 不加上任何dependency的話, 就是這個component每一次render結束後都會進來這個useEffect
    // 加上空陣列, 表示為ComponentDidMount的作用, 第一次render結束後會進來
    // 加上count, 表示count的值有變動的時候才會進來
    // useEffect return的東西是->這次的render結束之後最後執行的事情, 通常是clean up 某些東西的時機

    const increaseCount = () => {
        setCount(prevCount => prevCount+1);
    }

    const toggleLight = () => {
        setLightMode(prevLightMode => !prevLightMode);
    }

    const mouseOverHandler = event => {
        setMousePosition({x:event.pageX, y:event.pageY});
    }

    const style = {
        backgroundColor: lightMode ? 'pink':'grey',
        font: 'inherit',
        border: '1px solid black',
        padding: '8px',
        cursor: 'pointer'
      };

    return (
      <div>
        Function
        <br /><br/>
        <button onClick={increaseCount}>Click {count} times</button>
        <h2>Toggle light</h2>
        <button style={style} onClick={toggleLight}>mode</button>

        <h2>Mouse Position</h2>
        <p>{JSON.stringify(mousePosition)}</p>
      </div>
    )
}

export default App;